from fastapi import FastAPI
import entity.models as models
import config.database as database
import routers.admin as admin
import routers.transaction as transaction

route=FastAPI()
 
route.include_router(admin.router)
route.include_router(transaction.router)

models.Base.metadata.create_all(bind=database.engine) 
@route.get("/test")
def test():
    return "yes"