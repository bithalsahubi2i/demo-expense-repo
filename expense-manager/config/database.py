from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from urllib.parse import quote

CON_STRING="sqlite:///./expense.db"

engine=create_engine(
    CON_STRING
    ,connect_args = {"check_same_thread":False}
    ,encoding='latin1', echo=True
)
Base=declarative_base()
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

def get_db():
    session = SessionLocal()
    try:
        yield session
    finally:
        session.close()