from fastapi import APIRouter
import entity.schemas as schemas
import config.database as database
import services.users as users
from sqlalchemy.orm import Session
from fastapi import Depends, FastAPI
import entity.models as models
router = APIRouter()
get_db=database.get_db

@router.get("/users")
def get_users(session: Session=Depends(get_db)):
    result = users.get_all_users(session)
    return result

@router.get("/users/{user_id}")
def get_specific_user(user_id, session: Session=Depends(get_db)):
    result = users.get_users_by_id(session, user_id)
    return result

@router.post("/users")
async def create_user(request: schemas.Users, session: Session=Depends(get_db)):
    result=users.create_user(session,request)
    return {"message": "Successfully created", "users": result}


@router.put("/users/{user_id}")
def update_user(user_id, request: schemas.Users, session: Session=Depends(get_db)):
    result=users.update_user(session,request)
    return "successfull"

@router.delete("/users/{user_id}")
def delete_user(user_id, session: Session=Depends(get_db)):
    result=users.delete_user(session,user_id)
    return " Successfull"