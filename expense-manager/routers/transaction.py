from fastapi import APIRouter
import entity.schemas as schemas
import config.database as database
import services.users as users
from sqlalchemy.orm import Session
from fastapi import Depends, FastAPI
import entity.models as models
import services.transaction as transaction
router = APIRouter()
get_db=database.get_db

@router.get("/transactions/{created_by}")
def get_transactions_by_userid(created_by,session: Session=Depends(get_db)):
    result = transaction.get_transactions_by_userid(session,created_by)
    return result
    
@router.get("/transactions/{number_of_days}")
def get_last_transactions(number_of_days,session: Session=Depends(get_db)):
    result = transaction.get_last_transactions(session,number_of_days)
    return result

@router.get("/transactions/{page_num}")
def get_transactions_by_page_num(page_num):
    result = transaction.get_transactions_by_page_num(session,page_num)
    return result

@router.post("/transactions")
def create_new_transaction(request: schemas.TransactionsRequest,session: Session=Depends(get_db)):
    result=transaction.create_new_transaction(session,request)
    return {"message": "Successfully created", "transactions": result}

@router.put("/transactions/{transaction_id}")
def update_transaction(transaction_id, request: schemas.Transactions, session: Session=Depends(get_db)):
    result=transaction.update_transaction(session,request,transaction_id)
    return "successfull"

@router.delete("/transactions/{transaction_id}")
def delete_transaction(transaction_id,session: Session=Depends(get_db)):
    result=transaction.delete_transaction(session,transaction_id)
    return " Successfull"

