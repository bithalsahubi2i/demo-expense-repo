from sqlalchemy.orm import Session
from fastapi import FastAPI
import entity.models as models
import config.database as database
from datetime import date
from datetime import timedelta

today = date.today()

def create_new_transaction(session,request):
    today = date.today()
    transaction = models.Transactions(description=request.description,amount=request.amount,trans_type=request.trans_type,tag_id=request.tag_id,created_by=request.created_by,mode_id=request.mode_id,created_at=today)
    session.add(transaction)
    session.commit()
    session.refresh(transaction)
    return transaction

def get_transactions_by_userid(session, created_by):
        # Addl Business logic
        result = session.query(models.Transactions).filter(models.Transactions.created_by == created_by).all()
        return result

def get_last_transactions(number_of_days,session):
    today=date.today()
    d=date.today()-timedelta(days=number_of_days)
    #result= session.query(models.Transactions).filter(models.Transactions.created_by==1 and models.Transactions.created_at>"d").all()
    return d

#def get_transactions_by_page_num(session,page_num):
    #PAGE NUM*LIMIT-LIMIT=OFFSET
#LIMIT(PAGE NUM-1)=OFFSET


    
def update_transaction(session,request,transaction_id):
    result=session.query(models.Transactions).filter(models.Transactions.transaction_id == request.transaction_id  ).update({'description': request.description})
    session.commit()
    return result

def delete_transaction(session,transaction_id):
    result=session.query(models.Transactions).get(transaction_id)
    session.delete(result)
    session.commit()
    return result



