from sqlalchemy.orm import Session
from fastapi import Depends, FastAPI
import config.database as database
import entity.models as models

def get_all_users(session):
    users = session.query(models.Users).all()
    return users

def get_users_by_id(session, user_id):
        # Addl Business logic
        result = session.query(models.Users).filter(models.Users.user_id == user_id).all()
        return result

def create_user(session,request):
    users = models.Users(first_name=request.first_name, m_name=request.m_name, last_name=request.last_name, email=request.email, contact=request.contact)
    session.add(users)
    session.commit()
    session.refresh(users)
    return users

def update_user(session,request):
    result=session.query(models.Users).filter(models.Users.user_id == request.user_id  ).update({'email': request.email})
    session.commit()
    return result

def delete_user(session,user_id):
    users=session.query(models.Users).get(user_id)
    session.delete(users)
    session.commit()
    return users


    