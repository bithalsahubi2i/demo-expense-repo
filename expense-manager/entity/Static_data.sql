-- SQLite
INSERT INTO mode (mode)
VALUES ("Cash"),("Debit card"),("Credit card");

INSERT INTO tags (tag_name)
VALUES ("Food"),("Entertainment"),("Travel"),("Bills");

INSERT INTO transactions (description, amount, trans_type, tag_id, created_at, created_by, mode_id)
VALUES ("cinema",500.0,"expense",1,"2021-10-08",2,3),
("shopping",600.0,"expense",2,"2021-10-02",1,2),
("pocket money",2000.0,"income",1,"2021-10-05",5,1),
("cinema",500.0,"expense",1,"2021-10-01",2,3),
("stocks",5000.0,"income",1,"2021-10-03",4,2),
("food",700.0,"expense",1,"2021-10-04",3,2),
("games",800.0,"expense",2,"2021-10-08",1,1),
("salary",5000.0,"income",1,"2021-09-30",5,2);

SELECT transaction_id, description, amount, trans_type, tag_id, created_at, created_by, updated_at, mode_id
FROM transactions
WHERE created_by=1 and created_at>"2021-10-03";

SELECT transaction_id, description, amount, trans_type, tag_id, created_at, created_by, updated_at, mode_id
FROM transactions
where created_by=1
limit 5 offset 0;
