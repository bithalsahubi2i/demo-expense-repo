from typing import List
from typing import Optional
from pydantic import BaseModel
from datetime import datetime, time ,date
class Users(BaseModel):

    #user_id: int
    first_name: str
    m_name: Optional[str] = None
    last_name: Optional[str] = None
    email: str
    contact: Optional[int] = None

class Tags(BaseModel):
    #tag_id: int
    tag_name: str

class Mode(BaseModel):
    #mode_id: int
    mode: Optional[str] = None

class Transactions(BaseModel):
    transaction_id: int 
    description: str 
    amount: float
    trans_type: Optional[str] = None
    tag_id: int
    created_at: date
    created_by: int
    updated_at: Optional[date] = None
    mode_id: int

class TransactionsRequest(BaseModel):
    description: str 
    amount: float
    trans_type: Optional[str] = None
    tag_id: int
    created_by: int
    mode_id: int


