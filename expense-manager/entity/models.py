from sqlalchemy import Boolean, Column, ForeignKey, Integer, String,DATE
#from sqlalchemy.sql.sqltypes import Double
from sqlalchemy.sql.sqltypes import Float
from sqlalchemy.orm import relationship


import config.database as database
Base=database.Base

class Users(Base):
    __tablename__ = "users"
    __table_args__ = {'sqlite_autoincrement': True}
    
    user_id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    first_name = Column(String(1000), index=True)
    m_name = Column(String(1000), index=True)
    last_name = Column(String(1000), index=True)
    email = Column(String(1000), index=True)
    contact = Column(Integer, index=True)

    
class Tags(Base):
    __tablename__ = "tags"
    __table_args__ = {'sqlite_autoincrement': True}

    tag_id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    tag_name = Column(String(1000), index=True)
    

class Mode(Base):
    __tablename__ = "mode"
    __table_args__ = {'sqlite_autoincrement': True}

    mode_id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    mode = Column(String(255), index=True)
    
    

class Transactions(Base):
    __tablename__ = "transactions"
    __table_args__ = {'sqlite_autoincrement': True}

    transaction_id = Column(Integer, primary_key=True, index=True)
    description = Column(String(1000), index=True)
    amount = Column(Float, index=True)
    trans_type = Column(String(255), index=True)
    tag_id = Column(Integer, ForeignKey('tags.tag_id'))
    created_at = Column(DATE, index=True)
    created_by = Column(Integer, ForeignKey('users.user_id'))
    updated_at = Column(DATE, index=True)
    mode_id = Column(Integer, ForeignKey('mode.mode_id'))

    
    users = relationship("Users", back_populates = "transactions")
    
    tags = relationship("Tags", back_populates = "transactions")
    
    mode = relationship("Mode", back_populates = "transactions")

Users.transactions = relationship("Transactions", order_by = Transactions.transaction_id , back_populates = "users")
Tags.transactions = relationship("Transactions", order_by = Transactions.transaction_id , back_populates = "tags")
Mode.transactions = relationship("Transactions", order_by = Transactions.transaction_id , back_populates = "mode")









